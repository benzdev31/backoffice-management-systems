<?php


namespace App\Http\Controllers\Backoffice;


use App\Firebase\Firestore;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CustomerController extends Controller
{

    public function __construct(Request $request)
    {
        $this->db = new Firestore('customer');
    }

    /**
     *
     * @return View
     */
    public function index()
    {
        checkAuth();
        //echo extension_loaded('grpc') ? 'yes' : 'no';
//        $fireStore = new Firestore('customer');
        $db_customers = $this->db->getDocumentAll('crate_date');
        return view('backoffice.customer.index', compact('db_customers'));
    }

    /**
     * Show the form for creating
     *
     * @return Response|View
     */
    public function create()
    {

    }

    /**
     * Store a newly created Address
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        checkAuth();
        $save = $this->db->saveDocCollectionData($request->document, $request->collection, $request->dataVal);
        $status_code = null;
        if ($save) {
            $res = [
                'status' => true,
                'status_db' => $save,
                'data' => $request->input()
            ];
            $status_code = 200;
        } else {
            $res = [
                'status' => false,
                'status_db' => $save
            ];
            $status_code = 400;
        }
        return $this->sendResponse($res, $status_code);
    }

    /**
     * Display the specified
     *
     * @param int $id
     *
     * @return Response|View
     */
    public function show(string $id)
    {
        checkAuth();
        $db_car_type = $this->db->getDocCollection($id, 'car_type', '');
        $db_coupon = $this->db->getDocCollection($id, 'coupon', '');
        $db_transactions = $this->db->getDocCollection($id, 'transactions', 'date_in');
        $db_stamp = $this->db->getDocCollection($id, 'stamp', '');
        $db_objective = $this->db->getDocCollection($id, 'objective', '');
        $db_prefix_address = $this->db->getDocCollection($id, 'prefix_address', '');
        return view('backoffice.customer.show',
            compact('id',
                'db_car_type',
                'db_transactions',
                'db_stamp',
                'db_objective',
                'db_prefix_address',
                'db_coupon'));
    }

    /**
     * Show the form for editing the specified .
     *
     * @param int $id
     *
     * @return Response|View
     */
    public function edit(int $id)
    {

    }

    /**
     * Update the specified in storage.
     *
     * @param int $id
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function update(string $id, Request $request)
    {
        checkAuth();
        $update = $this->db->updateDocCollectionData($request->document, $request->collection, $request->dataVal, $id);
        $status_code = null;
        if ($update) {
            $res = [
                'status' => true,
                'status_db' => $update,
                'data' => $request->input()
            ];
            $status_code = 200;
        } else {
            $res = [
                'status' => false,
                'status_db' => $update
            ];
            $status_code = 400;
        }
        return $this->sendResponse($res, $status_code);
    }

    /**
     * Remove the specified from storage.
     *
     * @param int $id
     *
     * @return RedirectResponse|Response
     */
    public function destroy(int $id)
    {

    }

    /**
     * Update the specified in storage.
     *
     * @param Request $request
     *
     */
    public function deleteDocColData(Request $request)
    {
        checkAuth();
        $delete = $this->db->deleteDocCollectionData($request->document, $request->collection, $request->dataVal);
        $status_code = null;
        if ($delete) {
            $res = [
                'status' => true,
                'status_db' => $delete,
                'data' => $request->input()
            ];
            $status_code = 200;
        } else {
            $res = [
                'status' => false,
                'status_db' => $delete
            ];
            $status_code = 400;
        }
        return $this->sendResponse($res, $status_code);
    }

    public function saveDocument(Request $request)
    {
        checkAuth();
        $delete = $this->db->addDocumentData($request->customer_name);
        $status_code = null;
        if ($delete) {
            $res = [
                'status' => true,
                'status_db' => $delete,
                'data' => $request->input()
            ];
            $status_code = 200;
        } else {
            $res = [
                'status' => false,
                'status_db' => $delete
            ];
            $status_code = 400;
        }
        return $this->sendResponse($res, $status_code);
    }

    public function getViewDocCollection(Request $request)
    {
        checkAuth();
        $transactions_coll_db =$this->db->getDocCollectionData($request->document,$request->collection,$request->transactions_id);
        $status_code = null;
        if ($transactions_coll_db) {
//            if (key === 'date_in' || key === 'date_in') {
//
//            } else {
//
//            }
            $res = [
                'status' => true,
                'data' => $transactions_coll_db,
            ];
            $status_code = 200;
        } else {
            $res = [
                'status' => false,
                'data' => $transactions_coll_db
            ];
            $status_code = 400;
        }
        return $this->sendResponse($res, $status_code);
    }

}