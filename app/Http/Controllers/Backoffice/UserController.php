<?php


namespace App\Http\Controllers\Backoffice;


use App\Firebase\Firestore;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{

    public function __construct(Request $request)
    {
        $this->db = new Firestore('users');
    }

    /**
     *
     * @return View
     */
    public function index()
    {
        checkAuth();
        $db_users = $this->db->getDocumentAll();
        return view('backoffice.users.index', compact('db_users'));
    }

    /**
     * Show the form for creating
     *
     * @return Response|View
     */
    public function create()
    {

    }

    /**
     * Store a newly created Address
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        checkAuth();
        $delete = $this->db->addDocumentData($request->username);
        $status_code = null;
        if ($delete) {
            $res = [
                'status' => true,
                'status_db' => $delete,
                'data' => $request->input()
            ];
            $status_code = 200;
        } else {
            $res = [
                'status' => false,
                'status_db' => $delete
            ];
            $status_code = 400;
        }
        return $this->sendResponse($res, $status_code);
    }

    /**
     * Display the specified
     *
     * @param int $id
     *
     * @return Response|View
     */
    public function show(string $id)
    {
        checkAuth();
        $db_doc_user = $this->db->getDoc($id);
        $listColl_Customer = $this->db->getDocumentNameAll('customer');
        $listColl_Users = $this->db->getDocumentNameAll('users','customer_name','!=','');
        foreach ($listColl_Users as $key => $coll_User) {
            $listColl_Users[$key]["name"] = $listColl_Users[$key]["data"]["customer_name"];
        }
//        echo "Result = ".$this->searchForId("Qas",$listColl_Users);
        $items = collect($listColl_Customer)->filter(function ($item) use ($listColl_Users) {
            $index = $this->searchForId($item['id'],$listColl_Users);
            if ($index === null) {
                return $item['id'];
            }
        });
//        var_dump([count($listColl_Customer),count($listColl_Users)]);
//        dd($items);
        return view('backoffice.users.show',
            compact('id','db_doc_user','items'));
    }

    /**
     * Show the form for editing the specified .
     *
     * @param int $id
     *
     * @return Response|View
     */
    public function edit(int $id)
    {

    }

    /**
     * Update the specified in storage.
     *
     * @param int $id
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function update(int $id, Request $request)
    {

    }

    /**
     * Remove the specified from storage.
     *
     * @param int $id
     *
     * @return RedirectResponse|Response
     */
    public function destroy(int $id)
    {

    }

    function searchForId($id, $array) {
        foreach ($array as $key => $val) {
            if ($val['name'] === $id) {
                return $key;
            }
        }
        return null;
    }
}