<?php


namespace App\Firebase;

use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Firestore\FirestoreClient;
use http\Exception;

class Firestore
{
    public function __construct(string $collection)
    {
        $json = '{"type": "service_account","project_id": "v-car-park-dev","private_key_id": "20284496c714da217c30cb08f1dad2e6c6c0943e",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC5DwhLfCsVHBdC\nkW5m4QpLNMU+70L9g6xeDrD2r7pSmkhvVUDJmvfv+GSf5hg6P2DjuLbLBJlB0kCf\nB2iFBDkR17D60Z+9a6igC15zPsc2g7pgIAVUR97biGnrbGPdPuoY2OOCukLoBe6x\nRZROFz4QKxS4w3zSwtSEpv3H31BlK9Zm/WDAW54uYs1MAKKFP/7cp/LBSZ/CkxVI\npU74orp2alq64XUXX90n+cqQur5N7d9jGHY9kINQaX4sJ3QHJmoDWicllB3aO/59\nevU4BUmyxf/OyR7gOSYeGJrW25ex1uqnMXqurZ+FILDPoourbKPFgu9chAhkKY33\ncCZCYr+bAgMBAAECggEAAj7FNYW2bfsTrd99AKW1fOjro3nF46WgNAd2MGjxRTuj\n9XRr+3Ndsaai9TC8O1ZT+3x7GsplUOdqEPEK80shaS7FgPlJR56Ed4Otrdp/5Uzk\nqTYDp7bhKEzQRgwrfJbsDbIt0780D8eG4aiJGNqfsDLh18WqsUK9Za3nzqwZdF3u\nkQgL5/UNO+Vg7KAgtV228pNPgA5Qjx/X6K2UC8F/Ju1ZMt4pDX4jMssphGfD96kF\nywYKlKz7hFxMwbSztvY7uoSaD5rOcinegv696YYrLhm8QEcILPMROgGGrcLsCpHp\nrwi0tHKX4cAsDdsHgZWa5CAwKY4r12oBT2dG42QeeQKBgQDakfJbdfO/KJq+mc8c\n3hs+gOlo23jJDkHC5teZVcXuljdZwgA/kpXHczORk8co2DoKYD3Smyoqg6+t7v4w\nVAdn2ZVPJkniIHIalc6OsBuh0KZIaXVIozjlph+m0rJywGa/kUBWIcchbpozN8SX\nxqELvsr7PGGVCd+c8vPbVm5QtQKBgQDYv/SxUDvQZkS3NwjY03UakRHKIoS2ufIQ\n+ArNIHGJNMpmgsrZPtLqSDGjz7WqXTLOMnaniEC/W7MIiwYSn399QLYhjAzfijAY\nd4KYByN6OqdSJKdruw6qJO2nQhZrlEDoxy5rS52t38/UMWEo+MwL+4ACaZYxPLer\nNYAUzlARDwKBgEoy0XuLBzwVpLKftFewbr9BM/uMxew7Esf/ZaGAJLlh9VhbK4pp\nUL9stEzvReTvwoRTN6mPUzkrizAYueHeJMV0b+N2tA43xYSQ5pY54qeEFW7vngkS\nn/YV9xjC3iFmf3r47EZRITCQQ3ivreV4YKOEGZRAY0AdIfQ95drH9JshAoGAFWUV\nPS9jSBXgMImRKP3ucDnjnKU9nl/KYhxpzIeoTVfVprrAVSNbZF7N75KAJxnd+vaG\nOdE8OudNS8mVRt9RkUaC89rEJoiYaMHbNTB3Mwh5HaWalQq4ztioULs4RVQz+AY5\nMcQcA161QAfDxGySj5g6sYJEngWVR6QXSXZ39scCgYEAhIUmCeipKTfzi4f+w6v4\ny2PUzr0GlklPJTttm7le95TKvt0ffEMxQrxFU/cENlF/8OlWOLjDRlzodbK93Dkm\nttGH8f8IvwY8FKczOQHSU/xI6bUgge5wqjU2uHUfUctcLFFbqKiMZ7KaPl86oPpp\nTBxE4BpqIVtIOfodAou4/HA=\n-----END PRIVATE KEY-----\n",
  "client_email": "firebase-adminsdk-nkoiv@v-car-park-dev.iam.gserviceaccount.com",
  "client_id": "105700819236706126157",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-nkoiv%40v-car-park-dev.iam.gserviceaccount.com"}';
        try {
            $this->db = new FirestoreClient([
                'keyFile' => json_decode($json, true),
                "projectId" => "v-car-park-dev"
            ]);
        } catch (GoogleException $e) {
            echo $e->getMessage();
        }
        $this->name = $collection;
    }

    public function getDocumentAll(string $fieldOrderBy = null, string $orderBy = "desc")
    {
        try {
            $documents = $this->db->collection($this->name);
            if ($fieldOrderBy) {
                $documents = $this->db->collection($this->name)->orderBy($fieldOrderBy, $orderBy)->documents();
            } else {
                $documents = $this->db->collection($this->name)->documents();
            }
            if ($documents) {
                $arr = null;
                foreach ($documents as $key => $document) {
                    if ($document->exists()) {
                        $data = [
                            "id" => $document->id(),
                            'data' => $document->data()
                        ];
                        $arr[$key] = $data;
                    }
                }
                return $arr;
            } else {
                throw new \Exception('Document all are not exists');
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getDocumentNameAll(string $collection, string $field = null, string $operator = null, string $value = null)
    {
        try {
            if ($field !== null && $value !== null) {
                $documents = $this->db->collection($collection)->where($field, $operator, $value)->documents()->rows();
            } else {
                $documents = $this->db->collection($collection)->documents();
            }
            if ($documents) {
                $arr = null;
                foreach ($documents as $key => $document) {
                    if ($document->exists()) {
                        $data = [
                            "id" => $document->id(),
                            'data' => $document->data()
                        ];
                        $arr[$key] = $data;
                    }
                }
                return $arr;
            } else {
                throw new \Exception('Document all are not exists');
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getDoc(string $doc_name)
    {
        $arr = [];
        $query = $this->db->collection($this->name)->document($doc_name)->snapshot();
        if (!empty($query)) {
            $arr = [
                'id' => $query->id(),
                'data' => $query->data()
            ];
        }
        return $arr;
    }

    public function getWhere(string $field, string $operator, $value)
    {
        $arr = [];
        $query = $this->db->collection($this->name)->where($field, $operator, $value)->documents()->rows();
        if (!empty($query)) {
            foreach ($query as $value) {
                $arr[] = $value->data();
            }
        }
        return $arr;
    }

    public function updateCollection(string $doc_name, array $data = [])
    {
        // Update -> Collection -> Document (data Field)
        try {
            if ($doc_name) {
                $this->db->collection($this->name)->document($doc_name)->update([$data]);
                return true;
            } else {
                return false;
            }
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getDocCollection(string $doc_name, string $collection, string $ordeyBy)
    {
        $arr = [];
        $query = $this->db->collection($this->name)->document($doc_name);
        $docCollection = $query->collection($collection);
        if ($ordeyBy) {
            $docCollection = $docCollection->orderBy($ordeyBy, 'desc')->documents();
        } else {
            $docCollection = $docCollection->documents();
        }
        if (!empty($docCollection)) {
            foreach ($docCollection as $key => $collection) {
                if ($collection->data()) {
                    $arr[$key] = [
                        'id' => $collection->id(),
                        'data' => $collection->data()
                    ];
                } else {
                    $arr[$key] = [
                        'id' => $collection->id(),
                    ];
                }
            }
        }
        return $arr;
    }

    public function getDocCollectionData(string $doc_name, string $collection, string $doc_id)
    {
        $arr = [];
        $query = $this->db->collection($this->name)->document($doc_name)->collection($collection)->document($doc_id)->snapshot();
        if (!empty($query)) {
            $arr = [
                'id' => $query->id(),
                'data' => $query->data()
            ];
        }
        return $arr;
    }

    public function saveDocCollectionData(string $doc_name, string $collection, string $docId_val)
    {
        // Add -> Collection -> Document -> Collection -> (data : Document)
        try {
            if ($collection) {
                $this->db->collection($this->name)->document($doc_name)
                    ->collection($collection)->document($docId_val)->set([]);
                return true;
            } else {
                return false;
            }
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function updateDocCollectionData(string $doc_name, string $collection, string $docId_val, string $docId_old)
    {
        // Update -> Collection -> Document -> Collection -> (data : Document)
        try {
            if ($collection) {
                $this->db->collection($this->name)->document($doc_name)
                    ->collection($collection)->document($docId_val)->set([]);
                $this->deleteDocCollectionData($doc_name, $collection, $docId_old);
                return true;
            } else {
                return false;
            }
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function deleteDocCollectionData(string $doc_name, string $collection, string $docId_val)
    {
        // Delete -> Collection -> Document -> Collection -> (data : Document)
        try {
            if ($collection) {
                $this->db->collection($this->name)->document($doc_name)
                    ->collection($collection)->document($docId_val)->delete();
                return true;
            } else {
                return false;
            }
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function addDocumentData(string $doc_name)
    {
        // Add -> Collection -> Document -> (data : Document)
        try {
            if ($doc_name) {
                $this->db->collection($this->name)->document($doc_name)->create();
                return true;
            } else {
                return false;
            }
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

}