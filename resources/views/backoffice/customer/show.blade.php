@extends('layouts.backoffice.app')
@push('css_lib')
    @include('layouts.datatables_css')
    @include('layouts.custom_css')
{{--    <link rel="stylesheet" href="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css">--}}
@endpush
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-bold">ลูกค้า
                        <small class="mx-3">|</small><small>ข้อมูลโครงการ {{ $id }}</small>
                    </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb bg-white float-sm-right rounded-pill px-4 py-2 d-none d-md-flex">
                        <li class="breadcrumb-item"><a href="{{url('/backoffice-management-systems/customer')}}">
                                <i class="fas fa-folder-open"></i>
                                ข้อมูลโครงการ</a>
                        </li>
                        <li class="breadcrumb-item active">
                            รายการข้อมูล {{ $id }}
                        </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <div class="content">
        <div class="clearfix"></div>
        <div class="card shadow-sm">
            <div class="card-header">
                <ul class="nav nav-tabs  d-flex flex-md-row flex-column-reverse align-items-start card-header-tabs"
                    role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">
                            <i class="nav-icon fas fa-list-alt"></i> ประเภทรถ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">
                            <i class="nav-icon fas fa-list-alt"></i> คูปอง</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">
                            <i class="nav-icon fas fa-list-alt"></i> ข้อมูลรถเข้า-ออก</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-4" role="tab">
                            <i class="nav-icon fas fa-list-alt"></i> Stamp</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-5" role="tab">
                            <i class="nav-icon fas fa-list-alt"></i> วัตถุประสงค์</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-6" role="tab">
                            <i class="nav-icon fas fa-list-alt"></i> เลขที่บ้าน</a>
                    </li>
                </ul><!-- Tab panes -->
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tabs-1" role="tabpanel">
                        <button name="action"
                                title="เพิ่มข้อมูล"
                                onclick="createCarType()"
                                class="btn btn-success btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-plus-circle ew-icon"></i>
                                        </span>
                            เพิ่มข้อมูล
                        </button>
                        <br>
                        <br>
                        <table id="tb-car_type" class="table dataTable no-footer" style="width:100%">
                            <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>ประเภทรถ</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($db_car_type as $key => $item)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td class="animated_link2">
                                        {{  $item["id"]  }}
                                    </td>
                                    <td>
                                        <button name="action"
                                                title="แก้ไขข้อมูล"
                                                onclick="editCarType('{{ $item["id"] }}')"
                                                class="btn btn-primary btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-edit ew-icon"></i>
                                        </span>
                                            แก้ไขข้อมูล
                                        </button>
                                        <button name="action"
                                                title="ลบข้อมูล"
                                                onclick="deleteDB('{{ $item["id"] }}','car_type')"
                                                class="btn btn-danger btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-trash-restore ew-icon"></i>
                                        </span>
                                            ลบข้อมูล
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ลำดับ</th>
                                <th>ชื่อโครงการ</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                    <div class="tab-pane" id="tabs-2" role="tabpanel">
                        <button name="action"
                                title="เพิ่มข้อมูล"
                                onclick="createCoupon()"
                                class="btn btn-success btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-plus-circle ew-icon"></i>
                                        </span>
                            เพิ่มข้อมูล
                        </button>
                        <br>
                        <br>
                        <table id="tb-coupon" class="table dataTable no-footer" style="width:100%">
                            <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>ประเภทคูปอง</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($db_coupon as $key => $item)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td class="animated_link2">
                                        {{  $item["id"]  }}
                                    </td>
                                    <td>
                                        <button name="action"
                                                title="แก้ไขข้อมูล"
                                                onclick="editCoupon('{{ $item["id"] }}')"
                                                class="btn btn-primary btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-edit ew-icon"></i>
                                        </span>
                                            แก้ไขข้อมูล
                                        </button>
                                        <button name="action"
                                                title="ลบข้อมูล"
                                                onclick="deleteDB('{{ $item["id"] }}','coupon')"
                                                class="btn btn-danger btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-trash-restore ew-icon"></i>
                                        </span>
                                            ลบข้อมูล
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ลำดับ</th>
                                <th>ประเภทคูปอง</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                    <div class="tab-pane" id="tabs-3" role="tabpanel">
                        <table id="tb-transactions" class="table dataTable no-footer" style="width:100%">
                            <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>เวลาเข้า</th>
                                <th>เวลาออก</th>
                                <th>เลขที่ติดต่อ</th>
                                <th>ประเภทรถ</th>
                                <th>บัตรหาย</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($db_transactions as $key => $item)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ formatDate($item["data"]['date_in']) }}</td>
                                    <td>{{ formatDate($item["data"]['date_out']) }}</td>
                                    <td>{{ $item["data"]['license_plate_id'] }}</td>
                                    <td>
                                        {{ $item["data"]['car_type'] }}
                                    </td>
                                    <td>
                                        {{ ($item["data"]['card_missing'])?'บัตรหาย':'มีบัตร' }}
                                    </td>
                                    <td>
                                        <button name="action"
                                                onclick="viewDetailTransactions({{ $item["id"] }})"
                                                title="แก้ไขข้อมูล"
                                                class="btn btn-info btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-eye ew-icon"></i>
                                        </span>
                                            ดูข้อมูล
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ลำดับ</th>
                                <th>เวลาเข้า</th>
                                <th>เวลาออก</th>
                                <th>เลขที่ติดต่อ</th>
                                <th>ประเภทรถ</th>
                                <th>บัตรหาย</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                    <div class="tab-pane" id="tabs-4" role="tabpanel">
                        <button name="action"
                                title="เพิ่มข้อมูล"
                                onclick="createStamp()"
                                class="btn btn-success btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-plus-circle ew-icon"></i>
                                        </span>
                            เพิ่มข้อมูล
                        </button>
                        <br><br>
                        <table id="tb-stamp" class="table dataTable no-footer" style="width:100%">
                            <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>ประเภทแสตมป์</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($db_stamp as $key => $item)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td class="animated_link2">
                                        {{  $item["id"]  }}
                                    </td>
                                    <td>
                                        <button name="action"
                                                title="แก้ไขข้อมูล"
                                                onclick="editStamp('{{ $item["id"] }}')"
                                                class="btn btn-primary btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-edit ew-icon"></i>
                                        </span>
                                            แก้ไขข้อมูล
                                        </button>
                                        <button name="action"
                                                title="ลบข้อมูล"
                                                onclick="deleteDB('{{ $item["id"] }}','stamp')"
                                                class="btn btn-danger btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-trash-restore ew-icon"></i>
                                        </span>
                                            ลบข้อมูล
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ลำดับ</th>
                                <th>ประเภทแสตมป์</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                    <div class="tab-pane" id="tabs-5" role="tabpanel">
                        <button name="action"
                                title="เพิ่มข้อมูล"
                                onclick="createObjective()"
                                class="btn btn-success btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-plus-circle ew-icon"></i>
                                        </span>
                            เพิ่มข้อมูล
                        </button>
                        <br><br>
                        <table id="tb-objective" class="table dataTable no-footer" style="width:100%">
                            <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>วัตถุประสงค์</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($db_objective as $key => $item)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td class="animated_link2">
                                        {{  $item["id"]  }}
                                    </td>
                                    <td>
                                        <button name="action"
                                                title="แก้ไขข้อมูล"
                                                onclick="editObjective('{{ $item["id"] }}')"
                                                class="btn btn-primary btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-edit ew-icon"></i>
                                        </span>
                                            แก้ไขข้อมูล
                                        </button>
                                        <button name="action"
                                                title="ลบข้อมูล"
                                                onclick="deleteDB('{{ $item["id"] }}','objective')"
                                                class="btn btn-danger btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-trash-restore ew-icon"></i>
                                        </span>
                                            ลบข้อมูล
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ลำดับ</th>
                                <th>วัตถุประสงค์</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                    <div class="tab-pane" id="tabs-6" role="tabpanel">
                        <button name="action"
                                title="เพิ่มข้อมูล"
                                onclick="createPrefixAddress()"
                                class="btn btn-success btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-plus-circle ew-icon"></i>
                                        </span>
                            เพิ่มข้อมูล
                        </button>
                        <br><br>
                        <table id="tb-prefix_address" class="table dataTable no-footer" style="width:100%">
                            <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>เลขที่บ้าน</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($db_prefix_address as $key => $item)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td class="animated_link2">
                                        {{  $item["id"]  }}
                                    </td>
                                    <td>
                                        <button name="action"
                                                title="แก้ไขข้อมูล"
                                                onclick="editPrefixAddress('{{ $item["id"] }}')"
                                                class="btn btn-primary btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-edit ew-icon"></i>
                                        </span>
                                            แก้ไขข้อมูล
                                        </button>
                                        <button name="action"
                                                title="ลบข้อมูล"
                                                onclick="deleteDB('{{ $item["id"] }}','prefix_address')"
                                                class="btn btn-danger btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-trash-restore ew-icon"></i>
                                        </span>
                                            ลบข้อมูล
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ลำดับ</th>
                                <th>เลขที่บ้าน</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    {{--  Car Type  --}}
    <div class="modal fade" id="createCarTypeModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">เพิ่มข้อมูล</span>
                        <span class="fw-light">ประเภทรถ</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">ประเภทรถ
                                        <span style="color: red">*</span></label>
                                    <div class="col-sm-9">
                                        <input id="car_type_name" name="car_type_name" type="text" required
                                               placeholder="ประเภทรถ"
                                               class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" class="btn btn-outline-success" onclick="saveCarType()">
                        บันทึกข้อมูล
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editCarTypeModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">แก้ไขข้อมูล</span>
                        <span class="fw-light">ประเภทรถ</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">ประเภทรถ</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" id="field_id" name="field_id">
                                        <input id="edit_car_type_name" name="edit_car_type_name" type="text"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" class="btn btn-outline-success" onclick="updateCarType()">
                        บันทึกข้อมูล
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--  Coupon  --}}
    <div class="modal fade" id="createCouponModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">เพิ่มข้อมูล</span>
                        <span class="fw-light">คูปอง</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">คูปอง
                                        <span style="color: red">*</span></label>
                                    <div class="col-sm-9">
                                        <input id="coupon_name" name="coupon_name" type="text" required
                                               placeholder="คูปอง"
                                               class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" class="btn btn-outline-success" onclick="saveCoupon()">
                        บันทึกข้อมูล
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editCouponModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">แก้ไขข้อมูล</span>
                        <span class="fw-light">คูปอง</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">คูปอง</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" id="field_id" name="field_id">
                                        <input id="edit_coupon" name="edit_coupon" type="text"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" class="btn btn-outline-success" onclick="updateCoupon()">
                        บันทึกข้อมูล
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--  Stamp  --}}
    <div class="modal fade" id="createStampModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">เพิ่มข้อมูล</span>
                        <span class="fw-light">แสตมป์</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">แสตมป์
                                        <span style="color: red">*</span></label>
                                    <div class="col-sm-9">
                                        <input id="stamp_name" name="stamp_name" type="text" required
                                               placeholder="แสตมป์"
                                               class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" class="btn btn-outline-success" onclick="saveStamp()">
                        บันทึกข้อมูล
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editStampModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">แก้ไขข้อมูล</span>
                        <span class="fw-light">แสตมป์</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">แสตมป์</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" id="field_id" name="field_id">
                                        <input id="edit_stamp" name="edit_stamp" type="text"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" class="btn btn-outline-success" onclick="updateStamp()">
                        บันทึกข้อมูล
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--  objective  --}}
    <div class="modal fade" id="createObjectiveModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">เพิ่มข้อมูล</span>
                        <span class="fw-light">วัตถุประสงค์</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">วัตถุประสงค์
                                        <span style="color: red">*</span></label>
                                    <div class="col-sm-9">
                                        <input id="objective_name" name="objective_name" type="text" required
                                               placeholder="วัตถุประสงค์"
                                               class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" class="btn btn-outline-success" onclick="saveObjective()">
                        บันทึกข้อมูล
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editObjectiveModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">แก้ไขข้อมูล</span>
                        <span class="fw-light">วัตถุประสงค์</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">วัตถุประสงค์</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" id="field_id" name="field_id">
                                        <input id="edit_objective" name="edit_objective" type="text"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" class="btn btn-outline-success" onclick="updateObjective()">
                        บันทึกข้อมูล
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--  prefix_address  --}}
    <div class="modal fade" id="createPrefixAddressModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">เพิ่มข้อมูล</span>
                        <span class="fw-light">เลขที่บ้าน</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">เลขที่บ้าน
                                        <span style="color: red">*</span></label>
                                    <div class="col-sm-9">
                                        <input id="prefix_address_name" name="prefix_address_name" type="text" required
                                               placeholder="เลขที่บ้าน"
                                               class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" class="btn btn-outline-success" onclick="savePrefixAddress()">
                        บันทึกข้อมูล
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editPrefixAddressModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">แก้ไขข้อมูล</span>
                        <span class="fw-light">เลขที่บ้าน</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">เลขที่บ้าน</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" id="field_id" name="field_id">
                                        <input id="edit_prefix_address" name="edit_prefix_address" type="text"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" class="btn btn-outline-success" onclick="updatePrefixAddress()">
                        บันทึกข้อมูล
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--  transactions  --}}
    <div class="modal fade" id="viewTransactionsModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">ดูข้อมูล</span>
                        <span class="fw-light">ข้อมูลรถเข้า-ออก</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table>
                        <tbody>
                            <tr id="transactionsListDetails"></tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts_lib')
    @include('layouts.custom_js')
{{--    <script type="text/javascript"--}}
{{--            src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js"></script>--}}
    <script>
        //====================== CUSTOMER/SHOW/{id} ================================
        $(document).ready(function () {
            $('#tb-car_type').DataTable({
                "processing": true,
                "responsive": true,
                "autoWidth": false,
                "oLanguage": {
                    "sSearch": "ค้นหา:   "
                }
            });
            $('#tb-coupon').DataTable({
                "processing": true,
                "responsive": true,
                "autoWidth": false,
                "oLanguage": {
                    "sSearch": "ค้นหา:   "
                }
            });
            $('#tb-transactions').DataTable({
                "processing": true,
                "responsive": true,
                "autoWidth": false,
                "oLanguage": {
                    "sSearch": "ค้นหา:   "
                }
            });
            $('#tb-objective ').DataTable({
                "processing": true,
                "responsive": true,
                "autoWidth": false,
                "oLanguage": {
                    "sSearch": "ค้นหา:   "
                }
            });
            $('#tb-prefix_address').DataTable({
                "processing": true,
                "responsive": true,
                "autoWidth": false,
                "oLanguage": {
                    "sSearch": "ค้นหา:   "
                }
            });
            $('#tb-stamp').DataTable({
                "processing": true,
                "responsive": true,
                "autoWidth": false,
                "oLanguage": {
                    "sSearch": "ค้นหา:   "
                }
            });
        });

        //====================== CUSTOMER/SHOW/{id} ================================
        //***** start car type
        function createCarType() {
            $('#createCarTypeModal').modal('show');
        }

        function editCarType(value) {
            $('#field_id').val(value);
            $('#edit_car_type_name').val(value);
            $('#editCarTypeModal').modal('show');
        }

        function saveCarType() {
            const car_type_name = $("#car_type_name").val();
            if (car_type_name) {
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'document': '{{ $id }}',
                        'collection': 'car_type',
                        'dataVal': car_type_name,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
                $('#createCarTypeModal').modal('hide');

            } else {
                alert("กรุณากรอก ข้อมูล")
            }
        }

        function updateCarType() {
            const field_id = $("#field_id").val();
            const edit_car_type_name = $("#edit_car_type_name").val();
            if (edit_car_type_name) {
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer') }}' + '/' + field_id,
                    type: 'PUT',
                    dataType: 'json',
                    data: {
                        'document': '{{ $id }}',
                        'collection': 'car_type',
                        'dataVal': edit_car_type_name,
                        'field_id': field_id,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
                $('#editCarTypeModal').modal('hide');

            } else {
                alert("กรุณากรอก ข้อมูล")
            }
        }

        // end end car type *****
        //***** start coupon
        function createCoupon() {
            $('#createCouponModal').modal('show');
        }

        function editCoupon(value) {
            $('#field_id').val(value);
            $('#edit_coupon').val(value);
            $('#editCouponModal').modal('show');
        }

        function saveCoupon() {
            const coupon_name = $("#coupon_name").val();
            if (coupon_name) {
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'document': '{{ $id }}',
                        'collection': 'coupon',
                        'dataVal': coupon_name,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
                $('#createCouponModal').modal('hide');

            } else {
                alert("กรุณากรอก ข้อมูล")
            }
        }

        function updateCoupon() {
            const field_id = $("#field_id").val();
            const edit_coupon = $("#edit_coupon").val();
            if (edit_coupon) {
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer') }}' + '/' + field_id,
                    type: 'PUT',
                    dataType: 'json',
                    data: {
                        'document': '{{ $id }}',
                        'collection': 'coupon',
                        'dataVal': edit_coupon,
                        'field_id': field_id,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
                $('#editCouponModal').modal('hide');

            } else {
                alert("กรุณากรอก ข้อมูล")
            }
        }

        // end end coupon *****
        //***** start stamp
        function createStamp() {
            $('#createStampModal').modal('show');
        }

        function editStamp(value) {
            $('#field_id').val(value);
            $('#edit_stamp').val(value);
            $('#editStampModal').modal('show');
        }

        function saveStamp() {
            const stamp_name = $("#stamp_name").val();
            if (stamp_name) {
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'document': '{{ $id }}',
                        'collection': 'stamp',
                        'dataVal': stamp_name,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
                $('#createStampModal').modal('hide');

            } else {
                alert("กรุณากรอก ข้อมูล")
            }
        }

        function updateStamp() {
            const field_id = $("#field_id").val();
            const edit_stamp = $("#edit_stamp").val();
            if (edit_stamp) {
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer') }}' + '/' + field_id,
                    type: 'PUT',
                    dataType: 'json',
                    data: {
                        'document': '{{ $id }}',
                        'collection': 'coupon',
                        'dataVal': edit_stamp,
                        'field_id': field_id,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
                $('#editStampModal').modal('hide');

            } else {
                alert("กรุณากรอก ข้อมูล")
            }
        }

        // end end stamp *****
        //***** start objective
        function createObjective() {
            $('#createObjectiveModal').modal('show');
        }

        function editObjective(value) {
            $('#field_id').val(value);
            $('#edit_objective').val(value);
            $('#editObjectiveModal').modal('show');
        }

        function saveObjective() {
            const objective_name = $("#objective_name").val();
            if (objective_name) {
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'document': '{{ $id }}',
                        'collection': 'objective',
                        'dataVal': objective_name,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
                $('#createStampModal').modal('hide');

            } else {
                alert("กรุณากรอก ข้อมูล")
            }
        }

        function updateObjective() {
            const field_id = $("#field_id").val();
            const edit_objective = $("#edit_objective").val();
            if (edit_objective) {
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer') }}' + '/' + field_id,
                    type: 'PUT',
                    dataType: 'json',
                    data: {
                        'document': '{{ $id }}',
                        'collection': 'objective',
                        'dataVal': edit_objective,
                        'field_id': field_id,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
                $('#editObjectiveModal').modal('hide');
            } else {
                alert("กรุณากรอก ข้อมูล")
            }
        }
        // end end objective *****
        //***** start prefix_address
        function createPrefixAddress() {
            $('#createPrefixAddressModal').modal('show');
        }

        function editPrefixAddress(value) {
            $('#field_id').val(value);
            $('#edit_prefix_address').val(value);
            $('#editPrefixAddressModal').modal('show');
        }

        function savePrefixAddress() {
            const prefix_address_name = $("#prefix_address_name").val();
            if (prefix_address_name) {
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'document': '{{ $id }}',
                        'collection': 'prefix_address',
                        'dataVal': prefix_address_name,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
                $('#createPrefixAddressModal').modal('hide');

            } else {
                alert("กรุณากรอก ข้อมูล")
            }
        }

        function updatePrefixAddress() {
            const field_id = $("#field_id").val();
            const edit_prefix_address = $("#edit_prefix_address").val();
            if (edit_prefix_address) {
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer') }}' + '/' + field_id,
                    type: 'PUT',
                    dataType: 'json',
                    data: {
                        'document': '{{ $id }}',
                        'collection': 'prefix_address',
                        'dataVal': edit_prefix_address,
                        'field_id': field_id,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
                $('#editPrefixAddressModal').modal('hide');
            } else {
                alert("กรุณากรอก ข้อมูล")
            }
        }
        //***** start prefix_address
        //***** start transactions
        function viewDetailTransactions(id) {
            console.log(id)
            $('#viewTransactionsModal').modal('show');
            $.ajax({
                url: '{{ url('/backoffice-management-systems/customer/getViewDocCollection') }}',
                type: 'POST',
                dataType: 'json',
                data: {
                    'document': '{{ $id }}',
                    'collection': 'transactions',
                    'transactions_id': id,
                    "_token": "{{ csrf_token() }}",
                },
                success: function (response) {
                    const obj = response.data.data;
                    console.log(obj);
                    console.log(obj.data);
                    console.log(Object.keys(obj.data).length);
                    let html = "";
                    for (const key in obj.data) {
                        html += "<tr>" +
                            "<td>"+`${key}`+"</td>" +
                            "<td> : "+`${obj.data[key]}`+"</td>" +
                            "</tr>";
                        // console.log(`${key}: ${obj.data[key]}`);
                    }
                    console.log(html)
                    $("#transactionsListDetails").html(html);

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
            $('#createPrefixAddressModal').modal('hide');
        }
        //***** start transactions

        function deleteDB(doc_id, collection) {
            swal({
                    title: "ยืนยันการลบข้อมูล ?",
                    text: "ต้องการลบข้อมูลประเภทรถ " + doc_id + "!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'ใช่, ลบข้อมูล!',
                    cancelButtonText: "ไม่, ยกเลิก!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '{{ url('/backoffice-management-systems/customer/deleteDocColData') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                'document': '{{ $id }}',
                                'collection': collection,
                                'dataVal': doc_id,
                                "_token": "{{ csrf_token() }}",
                            },
                            success: function (response) {
                                if (response.data.status === true) {
                                    swal("ลบข้อมูล!", "ลบข้อมูลประเภทรถ " + doc_id + " เรียบร้อยแล้ว!", "success");
                                    window.setTimeout(function () {
                                        window.location.reload();
                                    }, 2000);
                                }
                            },
                            error: function (data) {
                                console.log('Error:', data);
                                swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                                window.setTimeout(function () {
                                    window.location.reload();
                                }, 2000);
                            }
                        });
                    } else {
                        swal("ยกเลิก", " :)", "error");
                    }
                });
        }

        function convertDate(date){
            // var d = new Date(date);
            console.log("convertDate = ",date.toString().toString() )
            // console.log(d.toDateString())
            // return d.getDate()+'/'+d.getMonth()+'/'+d.getFullYear();
        }
    </script>
@endpush

