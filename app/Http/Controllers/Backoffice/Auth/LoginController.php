<?php
/**
 * File name: Backoffice/Auth/LoginController.php
 * Last modified: 2021.05.11 at 15:00:00
 * Author: Mr. Nattapong Thipbamrung  (nattapong@koder3.com)
 * Copyright (c) 2021
 */

namespace App\Http\Controllers\Backoffice\Auth;


use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    protected $redirectTo = '/';

    public function pageLogin(Request $request)
    {
        if (session('user'))
        {
            return Redirect::to('backoffice-management-systems')->send();
        }
        return view('auth.backoffice.login');
    }

    public function login(Request $request)
    {
//        dd($request->input());
        $validator = Validator::make($request->all(), [ 'email' => 'required|email', 'password' => 'required', ]);
        if ($validator->fails()) {
            return redirect('/backoffice-management-systems/login')->with('error' , 'Username and Password required !');
//            return redirect()->with('error', 'Username and Password required');
        }
        if ($request->get('email') !== env('AUTH_LOGIN_USERNAME') &&
            $request->get('password') !== env('AUTH_LOGIN_PASSWORD')) {
            return redirect('/backoffice-management-systems/login')->with('error' , 'Username and Password ไม่ถูกต้อง !');
        } else {
            $data = $request->input();
            $request->session()->put('user',$data['email']);
            return redirect('/backoffice-management-systems')->with('success' , 'success');
        }
    }

    public function logout()
    {
        if (session()->has('user')) {
            session()->pull('user');
        }
        return redirect('/login');
    }
}