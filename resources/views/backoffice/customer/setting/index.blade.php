@extends('layouts.backoffice.app')
@push('css_lib')
    <style>
        .col-form-label2 {
            padding-top: calc(.375rem + 1px);
            padding-bottom: calc(.375rem + 1px);
            margin-bottom: 0;
            font-size: small;
            line-height: 1.5;
        }
    </style>
    @include('layouts.datatables_css')
    @include('layouts.custom_css')
    <style>
        #info-block section {
            border: 1px solid black;
        }

        .file-marker > div {
            padding: 0 3px;
            height: auto;
            margin-top: -0.8em;

        }

        .box-title {
            background: white none repeat scroll 0 0;
            display: inline-block;
            padding: 0 2px;
            margin-left: 8em;
        }

        /*  switch input  */
        /*
    get rid of the fieldset styling and keep
    this all on a single line
*/
        .radio-switch {
            border: none;
            padding: 0;
            white-space: nowrap;
        }

        /*
            radio button groups often benefit from a legend to
            provide context as to what the different
            options pertain to. Ideally this would be visible to all
            users, but you know...
        */
        .radio-switch legend {
            font-size: 2px;
            opacity: 0;
            position: absolute;
        }

        /*
            relative labels to help position the pseudo elements
            the z-index will be handy later, when the labels that
            overlap the visual switch UI need to be adjusted
            to allow for a user to toggle the switch without
            having to move their mouse/finger to the different
            sides of the UI
        */
        .radio-switch label {
            display: inline-block;
            line-height: 2;
            position: relative;
            z-index: 2;
        }

        /*
            inputs set to opcacity 0 are still accessible.
            Apparently there can be issues targetting inputs with
            Dragon speech recognition software if you use the typical
            'visually-hidden' class...so might as well just avoid that issue...
        */
        .radio-switch input {
            opacity: 0;
            position: absolute;
        }

        /*
            a 2 option toggle can only have 2 options...so instead of
            adding more classes, i'm just going to use some
            structural pseudo-classes to target them...
            cause why let all that good work go to waste?!

          the large padding is used to position the labels
          on top of the visual UI, so the switch UI itself
          can be mouse clicked or finger tapped to toggle
          the current option
        */
        .radio-switch label:first-of-type {
            padding-right: 5em;
        }

        .radio-switch label:last-child {
            margin-left: -4.25em;
            padding-left: 5em;
        }

        /*
            oh focus within, I can't wait for you to have even more support.
            But you'll never be in IE11, so we're going to need a
            polyfill for you for a bit...
         */
        .radio-switch:focus-within label:first-of-type:after {
            box-shadow: 0 0 0 2px #fff, 0 0 0 4px #2196f3;
        }

        /* polyfill class*/
        .radio-switch.focus-within label:first-of-type:after {
            box-shadow: 0 0 0 2px #fff, 0 0 0 4px #2196f3;
        }

        /* making the switch UI.  */
        .radio-switch label:first-of-type:before,
        .radio-switch label:first-of-type:after {
            border: 1px solid #aaa;
            content: "";
            height: 2em;
            overflow: hidden;
            pointer-events: none;
            position: absolute;
            vertical-align: middle;
        }

        .radio-switch label:first-of-type:before {
            background: #fff;
            border: 1px solid #aaa;
            border-radius: 100%;
            position: absolute;
            right: -.075em;
            transform: translateX(0em);
            transition: transform .2s ease-in-out;
            width: 2em;
            z-index: 2;
        }

        .radio-switch label:first-of-type:after {
            background: #222;
            border-radius: 1em;
            margin: 0 1em;
            transition: background .2s ease-in-out;
            width: 4em;
        }

        /*
            Visually change the switch UI to match the
            checked state of the first radio button
        */
        .radio-switch input:first-of-type:checked ~ label:first-of-type:after {
            background: #2196f3;
        }

        .radio-switch input:first-of-type:checked ~ label:first-of-type:before {
            transform: translateX(-2em);
        }

        /* Move the 2nd label to have a lower z-index, so when that
           option is toggled, the first label will overlay on top of the
           Switch ui, and the switch can be pressed again to toggle back
           to the prevoius state. */
        .radio-switch input:last-of-type:checked ~ label:last-of-type {
            z-index: 1;
        }

        /*
          system defaut fonts sure do load quick...
        */


    </style>
@endpush
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-bold">ลูกค้า
                        <small class="mx-3">|</small><small>Setting โครงการ {{ $id }}</small>
                    </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb bg-white float-sm-right rounded-pill px-4 py-2 d-none d-md-flex">
                        <li class="breadcrumb-item"><a href="{{url('/backoffice-management-systems/customer')}}">
                                <i class="fas fa-folder-open"></i>
                                ข้อมูลโครงการ</a>
                        </li>
                        <li class="breadcrumb-item active">
                            Setting โครงการ {{ $id }}
                        </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <div class="content">
        <div class="clearfix"></div>
        <div class="card shadow-sm">
            <div class="card-header">
                <ul class="nav nav-tabs  d-flex flex-md-row flex-column-reverse align-items-start card-header-tabs"
                    role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">
                            <i class="nav-icon fas fa-cog"></i> ตั้งค่าทั่วไป
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">
                            <i class="nav-icon fas fa-cog"></i> เปิด/ปิด การกรอกข้อมูล</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">
                            <i class="nav-icon fas fa-cog"></i> Fields เพิ่มเติม</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-4" role="tab">
                            <i class="nav-icon fas fa-cog"></i> สูตรค่าจอดรถ</a>
                    </li>
                </ul><!-- Tab panes -->
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tabs-1" role="tabpanel">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-6 col-form-label2">ชื่อโครงการ</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"
                                               value="{{ $id }}" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-6 col-form-label2">วันที่สร้าง</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"
                                               value="{{ formatDate($db_doc["data"]['crate_date']) }}"
                                               disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-6 col-form-label2">รหัสโครงการ
                                        (customer_code)</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"
                                               value="{{ $db_doc["data"]['customer_code'] }}"
                                               disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-6 col-form-label2">จำนวนเงินที่เก็บได้วานนี้
                                        (previous_date_fee_accumulate)</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"
                                               id="previous_date_fee_accumulate" name="previous_date_fee_accumulate"
                                               value="{{ $db_doc["data"]['previous_date_fee_accumulate'] }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-6 col-form-label2">จำนวนรถเข้า
                                        (in_count)</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"
                                               id="in_count" name="in_count"
                                               value="{{ $db_doc["data"]['in_count'] }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-6 col-form-label2">เลข
                                        PDF ล่าสุด (seq)</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"
                                               id="seq" name="seq"
                                               value="{{ $db_doc["data"]['seq'] }}">
                                    </div>
                                </div>
                            </div><!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-6 col-form-label2">เปิด/ปิด
                                        โครงการ (enable)</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="enable" name="enable">
                                            <option value="true"
                                                    {{ ($db_doc["data"]['enable'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                            <option value="false"
                                                    {{ ($db_doc["data"]['enable'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-6 col-form-label2">จำนวนเงินที่เก็บได้วันนี้
                                        (fee_accumulate)</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="fee_accumulate" name="fee_accumulate"
                                               value="{{ $db_doc["data"]['fee_accumulate'] }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-6 col-form-label2">พิน
                                        (pin)</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="pin" name="pin"
                                               value="{{ $db_doc["data"]['pin'] }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-6 col-form-label2">google_sheet_url</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="google_sheet_url" name="google_sheet_url"
                                               value="{{ $db_doc["data"]['google_sheet_url'] }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-6 col-form-label2">จำนวนรถออก
                                        (out_count)</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="out_count" name="out_count"
                                               value="{{ $db_doc["data"]['out_count'] }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-6 col-form-label2">ความสูงของใบเสร็จ
                                        (bill_height)</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="bill_height" name="bill_height"
                                               value="{{ $db_doc["data"]['bill_height'] }}">
                                    </div>
                                </div>
                            </div><!-- /.col -->
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-6 col-form-label2">เงื่อนไขค่าจอดรถ (info1)</label>
                                    <div class="col-sm-12">
                                                                <textarea type="text" class="form-control" name="info1"
                                                                          id="info1" style="min-height: 200px">{{ $db_doc["data"]['info1']  }}
                                                                </textarea>
                                    </div>
                                </div>
                                <br>
                                <div align="center">
                                    <button class="btn btn-success" type="button"
                                            onclick="saveDB(1)"
                                    ><i class="fas fa-save"></i>
                                        บันทึก
                                    </button>
                                </div>
                            </div>
                        </div><!-- /.row -->
                    </div>
                    <div class="tab-pane" id="tabs-2" role="tabpanel">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด/ปิด การขอความช่วยเหลือ (enable_activity)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_activity']))
                                            <select class="form-control" id="enable_activity" name="enable_activity">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_activity'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_activity'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_activity" name="enable_activity">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        จำนวนรูปที่ให้แนบ (enable_attatch_more_photo_count)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_attatch_more_photo_count']))
                                            <select class="form-control" id="enable_attatch_more_photo_count" name="enable_attatch_more_photo_count">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_attatch_more_photo_count'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_attatch_more_photo_count'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_attatch_more_photo_count" name="enable_attatch_more_photo_count">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เก็บรูปแนบบน Server (enable_attatch_more_photo_is_upload)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_attatch_more_photo_is_upload']))
                                            <select class="form-control" id="enable_attatch_more_photo_is_upload" name="enable_attatch_more_photo_is_upload">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_attatch_more_photo_is_upload'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_attatch_more_photo_is_upload'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_attatch_more_photo_is_upload" name="enable_attatch_more_photo_is_upload">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เข้ารหัสรูป (enable_camera_encrype)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_camera_encrype']))
                                            <select class="form-control" id="enable_camera_encrype" name="enable_camera_encrype">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_camera_encrype'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_camera_encrype'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_camera_encrype" name="enable_camera_encrype">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด / ปิด ประเภทรถ (enable_car_type)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_car_type']))
                                            <select class="form-control" id="enable_car_type" name="enable_car_type">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_car_type'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_car_type'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_car_type" name="enable_car_type">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด / ปิด บัตรสูญหาย (enable_card_missing)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_card_missing']))
                                            <select class="form-control" id="enable_card_missing" name="enable_card_missing">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_card_missing'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_card_missing'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_card_missing" name="enable_card_missing">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด / ปิด ที่อยู่ติดต่อ (enable_contact_address)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_contact_address']))
                                            <select class="form-control" id="enable_contact_address" name="enable_contact_address">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_contact_address'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_contact_address'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_contact_address" name="enable_contact_address">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด / ปิด การคิดเงิน (enable_fee)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_fee']))
                                            <select class="form-control" id="enable_fee" name="enable_fee">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_fee'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_fee'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_fee" name="enable_fee">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด / ปิด รองรับตัวหนังสือในทะเบียนรถ (enable_license_id_alpha)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_license_id_alpha']))
                                            <select class="form-control" id="enable_license_id_alpha" name="enable_license_id_alpha">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_license_id_alpha'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_license_id_alpha'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_license_id_alpha" name="enable_license_id_alpha">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด / ปิด ทะเบียนรถ (enable_license_plate_id)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_license_plate_id']))
                                            <select class="form-control" id="enable_license_plate_id" name="enable_license_plate_id">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_license_plate_id'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_license_plate_id'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_license_plate_id" name="enable_license_plate_id">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด/ปิด การแนบรูปเพิ่มเติม (enable_attatch_more_photo)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_attatch_more_photo']))
                                            <select class="form-control" id="enable_attatch_more_photo" name="enable_attatch_more_photo">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_attatch_more_photo'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_attatch_more_photo'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_attatch_more_photo" name="enable_attatch_more_photo">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        บังคับการแนบรูปเพิ่มเติม (enable_attatch_more_photo_is_required)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_attatch_more_photo_is_required']))
                                            <select class="form-control" id="enable_attatch_more_photo_is_required" name="enable_attatch_more_photo_is_required">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_attatch_more_photo_is_required'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_attatch_more_photo_is_required'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_attatch_more_photo_is_required" name="enable_attatch_more_photo_is_required">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด / ปิด การใช้งานกล้อง (enable_camera)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_camera']))
                                            <select class="form-control" id="enable_camera" name="enable_camera">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_camera'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_camera'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_camera" name="enable_camera">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิดปิดการใส่ลายน้ำที่รูปภาพ (enable_camera_watermark)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_camera_watermark']))
                                            <select class="form-control" id="enable_camera_watermark" name="enable_camera_watermark">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_camera_watermark'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_camera_watermark'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_camera_watermark" name="enable_camera_watermark">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด / ปิด วัตถุประสงค์ (enable_objective)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_objective']))
                                            <select class="form-control" id="enable_objective" name="enable_objective">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_objective'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_objective'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_objective" name="enable_objective">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด / ปิด ปุ่มพิมพ์ใบเสร็จ (enable_print_button)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_print_button']))
                                            <select class="form-control" id="enable_print_button" name="enable_print_button">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_print_button'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_print_button'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_print_button" name="enable_print_button">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด / ปิด ระบบแจ้งเตือนโครงการอื่น (enable_send_notify_to_mooban)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_send_notify_to_mooban']))
                                            <select class="form-control" id="enable_send_notify_to_mooban" name="enable_send_notify_to_mooban">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_send_notify_to_mooban'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_send_notify_to_mooban'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_send_notify_to_mooban" name="enable_send_notify_to_mooban">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด / ปิด เซ็นชื่อ (enable_signpad)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_signpad']))
                                            <select class="form-control" id="enable_signpad" name="enable_signpad">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_signpad'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_signpad'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_signpad" name="enable_signpad">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด / ปิด การแสตมป์ เข้า-ออก (enable_stamp)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_stamp']))
                                            <select class="form-control" id="enable_stamp" name="enable_stamp">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_stamp'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_stamp'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_stamp" name="enable_stamp">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label  class="col-sm-6 col-form-label2">
                                        เปิด / ปิด ใช้งานคูปอง (enable_use_coupon)
                                    </label>
                                    <div class="col-sm-6">
                                        @if(!empty($db_doc["data"]['enable_use_coupon']))
                                            <select class="form-control" id="enable_use_coupon" name="enable_use_coupon">
                                                <option value="true"
                                                        {{ ($db_doc["data"]['enable_use_coupon'] === 'true') ? 'selected' : '' }}>เปิด</option>
                                                <option value="false"
                                                        {{ ($db_doc["data"]['enable_use_coupon'] === 'false') ? 'selected' : '' }}>ปิด</option>
                                            </select>
                                        @else
                                            <select class="form-control" id="enable_use_coupon" name="enable_use_coupon">
                                                <option value="true">เปิด</option>
                                                <option value="false" selected>ปิด</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-12">
                                <br>
                                <div align="center">
                                    <button class="btn btn-success" type="button"
                                            onclick="saveDB(2)"
                                    ><i class="fas fa-save"></i>
                                        บันทึก
                                    </button>
                                </div>
                            </div>
                        </div><!-- /.row -->
                    </div>
                    <div class="tab-pane" id="tabs-3" role="tabpanel">
                        <div class="row mb-2">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="inputPassword"
                                           class="col-sm-6 col-form-label2">Fields เพิ่มเติม (addition_fields)</label>
                                    <div class="col-sm-12">
                                                                <textarea type="text" class="form-control" name="addition_fields"
                                                                          id="addition_fields" style="min-height: 200px">{{ $db_doc["data"]['addition_fields']  }}
                                                                </textarea>
                                    </div>
                                </div>
                                <br>
                                <div align="center">
                                    <button class="btn btn-success" type="button"
                                            onclick="saveDB(3)"
                                    ><i class="fas fa-save"></i>
                                        บันทึก
                                    </button>
                                </div>
                            </div>
                        </div><!-- /.row -->
                    </div>
                    <div class="tab-pane" id="tabs-4" role="tabpanel">
                        <div class="row mb-2">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="inputPassword"
                                           class="col-sm-6 col-form-label2">สูตรค่าจอดรถ (calculation_js)</label>
                                    <div class="col-sm-12">
                                                                <textarea type="text" class="form-control" name="calculation_js"
                                                                          id="calculation_js" style="min-height: 200px">{{ $db_doc["data"]['calculation_js']  }}
                                                                </textarea>
                                    </div>
                                </div>
                                <br>
                                <div align="center">
                                    <button class="btn btn-success" type="button"
                                            onclick="saveDB(4)"
                                    ><i class="fas fa-save"></i>
                                        บันทึก
                                    </button>
                                </div>
                            </div>
                        </div><!-- /.row -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts_lib')
    @include('layouts.custom_js')
    <script>
        //====================== CUSTOMER/SETTING ================================
        function saveDB(tab) {
            if (tab == 1) {
                const previous_date_fee_accumulate = $("#previous_date_fee_accumulate").val();
                const in_count = $("#in_count").val();
                const seq = $("#seq").val();
                const enable = $("#enable").val();
                const fee_accumulate = $("#fee_accumulate").val();
                const pin = $("#pin").val();
                const google_sheet_url = $("#google_sheet_url").val();
                const out_count = $("#out_count").val();
                const bill_height = $("#bill_height").val();
                const info1 = $("#info1").val();
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer/setting/update') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        '_tab': 1,
                        "_token": "{{ csrf_token() }}",
                        'id': '{{ $id }}',
                        'previous_date_fee_accumulate': previous_date_fee_accumulate,
                        'in_count': in_count,
                        'seq': seq,
                        'enable': enable,
                        'fee_accumulate': fee_accumulate,
                        'pin': pin,
                        'google_sheet_url': google_sheet_url,
                        'out_count': out_count,
                        'bill_height': bill_height,
                        'info1': info1
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
            } else if (tab == 2) {
                const enable_activity = $("#enable_activity").val();
                const enable_attatch_more_photo_count = $("#enable_attatch_more_photo_count").val();
                const enable_attatch_more_photo_is_upload = $("#enable_attatch_more_photo_is_upload").val();
                const enable_camera_encrype = $("#enable_camera_encrype").val();
                const enable_car_type = $("#enable_car_type").val();
                const enable_card_missing = $("#enable_card_missing").val();
                const enable_contact_address = $("#enable_contact_address").val();
                const enable_fee = $("#enable_fee").val();
                const enable_license_id_alpha = $("#enable_license_id_alpha").val();
                const enable_license_plate_id = $("#enable_license_plate_id").val()
                const enable_attatch_more_photo = $("#enable_attatch_more_photo").val();
                const enable_attatch_more_photo_is_required = $("#enable_attatch_more_photo_is_required").val();
                const enable_camera = $("#enable_camera").val();
                const enable_camera_watermark = $("#enable_camera_watermark").val();
                const enable_objective = $("#enable_objective").val()
                const enable_print_button = $("#enable_print_button").val();
                const enable_send_notify_to_mooban = $("#enable_send_notify_to_mooban").val();
                const enable_signpad = $("#enable_signpad").val();
                const enable_stamp = $("#enable_stamp").val();
                const enable_use_coupon = $("#enable_use_coupon").val()
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer/setting/update') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        '_tab': 2,
                        "_token": "{{ csrf_token() }}",
                        'id': '{{ $id }}',
                        'enable_activity': enable_activity,
                        'enable_attatch_more_photo_count': enable_attatch_more_photo_count,
                        'enable_attatch_more_photo_is_upload': enable_attatch_more_photo_is_upload,
                        'enable_camera_encrype': enable_camera_encrype,
                        'enable_car_type': enable_car_type,
                        'enable_card_missing': enable_card_missing,
                        'enable_contact_address': enable_contact_address,
                        'enable_fee': enable_fee,
                        'enable_license_id_alpha': enable_license_id_alpha,
                        'enable_license_plate_id': enable_license_plate_id,
                        'enable_attatch_more_photo': enable_attatch_more_photo,
                        'enable_attatch_more_photo_is_required': enable_attatch_more_photo_is_required,
                        'enable_camera': enable_camera,
                        'enable_camera_watermark': enable_camera_watermark,
                        'enable_objective': enable_objective,
                        'enable_print_button': enable_print_button,
                        'enable_send_notify_to_mooban': enable_send_notify_to_mooban,
                        'enable_signpad': enable_signpad,
                        'enable_stamp': enable_stamp,
                        'enable_use_coupon': enable_use_coupon
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
            } else if (tab == 3) {
                const addition_fields = $("#addition_fields").val();
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer/setting/update') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        '_tab': 3,
                        "_token": "{{ csrf_token() }}",
                        'id': '{{ $id }}',
                        'addition_fields': addition_fields
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
            } else if (tab == 4) {
                const calculation_js = $("#calculation_js").val();
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer/setting/update') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        '_tab': 4,
                        "_token": "{{ csrf_token() }}",
                        'id': '{{ $id }}',
                        'calculation_js': calculation_js
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
            }
        }

        // script **
        /**
         Focus within pollyfill
         Credit: https://gist.github.com/aFarkas/a7e0d85450f323d5e164
         */
        (function (window, document) {
            'use strict';
            var slice = [].slice;
            var removeClass = function (elem) {
                elem.classList.remove('focus-within');
            };
            var update = (function () {
                var running, last;
                var action = function () {
                    var element = document.activeElement;
                    running = false;
                    if (last !== element) {
                        last = element;
                        slice.call(document.getElementsByClassName('focus-within')).forEach(removeClass);
                        while (element && element.classList) {
                            element.classList.add('focus-within');
                            element = element.parentNode;
                        }
                    }
                };
                return function () {
                    if (!running) {
                        requestAnimationFrame(action);
                        running = true;
                    }
                };
            })();
            document.addEventListener('focus', update, true);
            document.addEventListener('blur', update, true);
            update();
        })(window, document);
        //====================== CUSTOMER/SETTING/ ================================
    </script>
@endpush
