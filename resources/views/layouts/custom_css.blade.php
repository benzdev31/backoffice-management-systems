<style>
    .page-item:first-child .page-link {
        margin-left: 0;
        border-top-left-radius: .25rem;
        border-bottom-left-radius: .25rem;
        width: auto !important;
    }

    .page-item:last-child .page-link {
        border-top-right-radius: .25rem;
        border-bottom-right-radius: .25rem;
        width: auto !important;
    }

    .animated_link2 a {
        position: relative;
        color: white;
    }

    .animated_link2 a:hover {
        color: white;
    }

    .animated_link2 a:before {
        content: "";
        position: absolute;
        width: 100%;
        height: 2px;
        bottom: 0;
        left: 0;
        background-color: #333;
        visibility: hidden;
        -webkit-transform: scaleX(0);
        transform: scaleX(0);
        -webkit-transition: all 0.3s ease-in-out 0s;
        transition: all 0.3s ease-in-out 0s;
    }

    .animated_link2 a:hover:before {
        visibility: visible;
        background-color: #f2f2f2;
        -webkit-transform: scaleX(1);
        transform: scaleX(1);
    }
    .alert2{
        position: absolute;
        top: 5px;
        right:2%;
        width: auto !important;
        padding: .75rem 1.25rem;
        margin-bottom: 1rem;
        border: 1px solid transparent;
        border-radius: .25rem;
    }
    .alert-success-custom {
        color: #20993c;
        background-color: #d4edda;
        border-color: #c3e6cb;
    }
    .alert3{
        position: absolute;
        top: 5px;
        right:2%;
        width: auto !important;
        padding: .75rem 1.25rem;
        margin-bottom: 1rem;
        border: 1px solid transparent;
        border-radius: .25rem;
    }
    .alert-danger-custom {
        color: #721c24;
        background-color: #f8d7da;
        border-color: #f5c6cb;
    }
</style>