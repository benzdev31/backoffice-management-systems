@extends('layouts.backoffice.app')
@push('css_lib')
    @include('layouts.datatables_css')
    @include('layouts.custom_css')
@endpush
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-bold">ลูกค้า
                        <small class="mx-3">|</small><small>ข้อมูลโครงการ</small>
                    </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb bg-white float-sm-right rounded-pill px-4 py-2 d-none d-md-flex">
                        <li class="breadcrumb-item active"><a href="{{url('/backoffice-management-systems')}}">
                                <i class="fas fa-folder-open"></i>
                                ข้อมูลโครงการ</a>
                        </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="alert2 alert-success-custom fade show alert-dismissible" id="alert-success" role="alert" style="display: none;">
        <strong><i class="fa fa-check-circle" aria-hidden="true"></i></strong>
        Update data <b>success</b> message!
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="alert3 alert-danger-custom fade show alert-dismissible" id="alert-danger" role="alert" style="display: none;">
        <strong><i class="fa fa-warning" aria-hidden="true"></i></strong>
        Update data <b>Fail</b> message!
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="content">
        <div class="clearfix"></div>
        <div class="card shadow-sm">
            <div class="card-header">
                <ul class="nav nav-tabs d-flex flex-md-row flex-column-reverse align-items-start card-header-tabs">
                    <div class="d-flex flex-row">
                        <li class="nav-item">
                            <a class="nav-link active" href="{!! url()->current() !!}">
                                <i class="fa fa-list mr-2"></i>โครงการทั้งหมด</a>
                        </li>
                    </div>
                </ul>
            </div>
            <div class="card-body">
                <button name="action"
                        title="เพิ่มลูกค้า"
                        onclick="viewCreateCustomer()"
                        class="btn btn-success btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-plus-circle ew-icon"></i>
                                        </span>
                    เพิ่มลูกค้า
                </button>
                <br>
                <br>
                <table id="tb-customer" class="table dataTable no-footer" style="width:100%">
                    <thead>
                    <tr>
                        <th>ลำดับ</th>
                        <th>ชื่อโครงการ</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($db_customers as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td class="animated_link2">
                                <a href="{{ url('/backoffice-management-systems/customer/'. $item["id"]) }}" style="color: black">
                                    {{  $item["id"]  }}
                                </a>
                            </td>
                            <td>
                                <a href="{{ url('/backoffice-management-systems/customer/setting/'. $item["id"]) }}"
                                        class="btn btn-info btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-edit ew-icon"></i>
                                        </span>
                                    Setting
                                </a>
                                <a href="{{ url('/backoffice-management-systems/customer/'. $item["id"]) }}"
                                   class="btn btn-warning btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-folder ew-icon"></i>
                                        </span>
                                    แฟ้มข้อมูล
                                </a>
{{--                                <button name="action"--}}
{{--                                        onclick="viewModelDetail('{{ $item["id"] }}')"--}}
{{--                                        class="btn btn-info btn-xs">--}}
{{--                                        <span class="btn-label">--}}
{{--                                        <i class="fas fa-eye ew-icon"></i>--}}
{{--                                        </span>--}}
{{--                                    ดูข้อมูล--}}
{{--                                </button>--}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>ลำดับ</th>
                        <th>ชื่อโครงการ</th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="viewRowModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
						<span class="fw-mediumbold">ดูข้อมูล</span>
                        <span class="fw-light">Customer</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>customer_code</label>
                                    <select class="custom-select form-control-border"
                                            onchange="viewContentField(this)"
                                            id="selectListField">
                                    </select>
                                </div>
                                <hr>
                                <div id="contentEdit" style="display: none;">
                                    <div class="form-group" align="center">
                                        <input id="field_name" name="field_name" type="hidden" class="form-control"
                                               value="" disabled>
                                        <input id="field_value" name="field_value" type="text" class="form-control"
                                               value="" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer no-bd">
                    <div id="buttonSave" style="display: none;">
                        <button type="button" class="btn btn-outline-success" onclick="saveCollectionDB()">
                            บันทึกข้อมูล
                        </button>
                    </div>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Create customer   --}}
    <div class="modal fade" id="createCustomerModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">เพิ่มโครงการ</span>
                        <span class="fw-light">ลูกค้า</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group" align="center">
                                    <input id="customer_name" name="customer_name" type="text" class="form-control"
                                        placeholder="กรุณากรอกข้อมูลโครงการ ลูกค้า">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" class="btn btn-outline-success" onclick="addCustomerDB()">
                        บันทึกข้อมูล
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts_lib')
    @include('layouts.custom_js')
    <script>
        //====================== CUSTOMER ================================
        $(document).ready(function () {
            $('#tb-customer').DataTable({
                "processing": true,
                "responsive": true,
                "autoWidth": false,
                "oLanguage": {
                    "sSearch": "ค้นหา:   "
                }
            });
        });

        let dataList = [];
        let keyDataId = '';
        let indexArr = null;

        function viewModelDetail(id)
        {
            const myElement = document.getElementById('contentEdit');
            if (myElement) myElement.style.display = "none";
            const myElement2 = document.getElementById('buttonSave');
            if (myElement2) myElement2.style.display = "none";

            $.ajax({
                url: '{{ url('/api/collection/customers') }}',
                type: 'POST',
                dataType: 'json',
                data: {
                    'name': id,
                    'collection': 'customer',
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function(res){
                var options = [];
                options += '<option selected disabled>กรุณาเลือก</option>';
                for (var i = 0; i < res.data.data.length; i++) {
                    options += '<option value="' +i+ '">' + res.data.data[i].name + '</option>';
                    let my_object = {};
                    my_object.name = res.data.data[i].name;
                    my_object.value = res.data.data[i].value;
                    dataList.push(my_object);
                }
                $("#selectListField").html(options);
                keyDataId = id;
            });
            $('#viewRowModal').modal('show');
        }

        // View Field value db firestore
        function viewContentField(object)
        {
            const index = object.value;
            // console.log(index);
            // console.log(dataList[index]);
            $("#field_name").val(dataList[index].name);
            $("#field_value").val(dataList[index].value);
            indexArr = index;
            document.getElementById('field_value').disabled = false;
            const myElement = document.getElementById('contentEdit');
            if (myElement) myElement.style.display = "block";
            const myElement2 = document.getElementById('buttonSave');
            if (myElement2) myElement2.style.display = "block";
        }
        // save or update collection db firestore
        function saveCollectionDB()
        {
            var a = document.getElementById("alert-success");
            a.style.display = "none";
            a.append();
            const field_name = $("#field_name").val();
            const field_value = $("#field_value").val()
            if (field_value) {
                dataList[indexArr].value = field_value;
                $.ajax({
                    url: '{{ url('/api/collection/customers') }}'+'/'+keyDataId,
                    type: 'PUT',
                    dataType: 'json',
                    data: {
                        'data': dataList,
                        'field_id': keyDataId,
                        'field_name': field_name,
                        'field_value': field_value,
                        {{--"_token": "{{ csrf_token() }}",--}}
                    },
                    success: function(response){
                        if (response.data.status === true) {
                            var alert = document.getElementById("alert-success");
                            console.log(alert)
                            alert.style.display = "block";
                            window.setTimeout(function() {
                                $(".alert2").fadeTo(500, 0).slideUp(500, function(){
                                    // $(this).hide();
                                });
                            }, 3000);
                        } else {
                            var alert = document.getElementById("alert-danger");
                            alert.style.display = "block";
                            window.setTimeout(function() {
                                $(".alert3").fadeTo(500, 0).slideUp(500, function(){
                                    $(this).hide();
                                });
                            }, 3000);
                        }
                    }
                });
                closeModelDetail();
            }
        }
        // close model and clear js, data
        function closeModelDetail()
        {
            var options = '';
            options += '<option selected disabled>กรุณาเลือก</option>';
            $("#selectListField").html(options);
            const myElement = document.getElementById('contentEdit');
            if (myElement) myElement.style.display = "none";
            const myElement2 = document.getElementById('buttonSave');
            if (myElement2) myElement2.style.display = "none";
            dataList = [];
            keyDataId = '';
            $("#field_name").val("");
            $("#field_value").val("")
            $('#viewRowModal').modal('hide');
        }

        function viewCreateCustomer()
        {
            $('#createCustomerModal').modal('show');
        }
        function addCustomerDB()
        {
            const customer_name = $("#customer_name").val();
            if (customer_name) {
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/customer/saveDocument') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'customer_name': customer_name,
                        'collection': 'customer',
                        "_token": "{{ csrf_token() }}",
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
                $('#createCustomerModal').modal('hide');
            } else {
                alert("กรุณากรอกข้อมูล !")
            }

        }

        //====================== CUSTOMER ================================
    </script>
@endpush

