<?php


namespace App\Http\Controllers\Backoffice;

use App\Firebase\Firestore;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SettingCustomerController extends Controller
{
    public function __construct(Request $request)
    {
        $this->db = new Firestore('customer');
    }


    public function index(string $id)
    {
        checkAuth();
        $db_doc = $this->db->getDoc($id);
        return view('backoffice.customer.setting.index', compact('id', 'db_doc'));
    }

    public function store(Request $request)
    {
        $_id = $request->id;
        if ($request->_tab === "1") {
            $enable = ['path' => 'enable', 'value' => $request->enable];
            $this->db->updateCollection($_id, $enable);
            $previous_date_fee_accumulate = ['path' => 'previous_date_fee_accumulate', 'value' => $request->previous_date_fee_accumulate];
            $this->db->updateCollection($_id, $previous_date_fee_accumulate);
            $in_count = ['path' => 'in_count', 'value' => $request->in_count];
            $this->db->updateCollection($_id, $in_count);
            $seq = ['path' => 'seq', 'value' => $request->seq];
            $this->db->updateCollection($_id, $seq);
            $fee_accumulate = ['path' => 'fee_accumulate', 'value' => $request->fee_accumulate];
            $this->db->updateCollection($_id, $fee_accumulate);
            $pin = ['path' => 'pin', 'value' => $request->pin];
            $this->db->updateCollection($_id, $pin);
            $google_sheet_url = ['path' => 'google_sheet_url', 'value' => $request->google_sheet_url];
            $this->db->updateCollection($_id, $google_sheet_url);
            $out_count = ['path' => 'out_count', 'value' => $request->out_count];
            $this->db->updateCollection($_id, $out_count);
            $bill_height = ['path' => 'bill_height', 'value' => $request->bill_height];
            $this->db->updateCollection($_id, $bill_height);
            $info1 = ['path' => 'info1', 'value' => $request->info1];
            $this->db->updateCollection($_id, $info1);
            $res = [
                'status' => true,
                'data' => $request->input()
            ];
            $status_code = 200;
        } else if ($request->_tab === "2") {
            $enable_activity = ['path' => 'enable_activity', 'value' => $request->enable_activity];
            $this->db->updateCollection($_id, $enable_activity);
            $enable_attatch_more_photo_count = ['path' => 'enable_attatch_more_photo_count', 'value' => $request->enable_attatch_more_photo_count];
            $this->db->updateCollection($_id, $enable_attatch_more_photo_count);
            $enable_attatch_more_photo_is_upload = ['path' => 'enable_attatch_more_photo_is_upload', 'value' => $request->enable_attatch_more_photo_is_upload];
            $this->db->updateCollection($_id, $enable_attatch_more_photo_is_upload);
            $enable_camera_encrype = ['path' => 'enable_camera_encrype', 'value' => $request->enable_camera_encrype];
            $this->db->updateCollection($_id, $enable_camera_encrype);
            $enable_car_type = ['path' => 'enable_car_type', 'value' => $request->enable_car_type];
            $this->db->updateCollection($_id, $enable_car_type);
            $enable_card_missing = ['path' => 'enable_card_missing', 'value' => $request->enable_card_missing];
            $this->db->updateCollection($_id, $enable_card_missing);
            $enable_contact_address = ['path' => 'enable_contact_address', 'value' => $request->enable_contact_address];
            $this->db->updateCollection($_id, $enable_contact_address);
            $enable_fee = ['path' => 'enable_fee', 'value' => $request->enable_fee];
            $this->db->updateCollection($_id, $enable_fee);
            $enable_license_id_alpha = ['path' => 'enable_license_id_alpha', 'value' => $request->enable_license_id_alpha];
            $this->db->updateCollection($_id, $enable_license_id_alpha);
            $enable_license_plate_id = ['path' => 'enable_license_plate_id', 'value' => $request->enable_license_plate_id];
            $this->db->updateCollection($_id, $enable_license_plate_id);
            $enable_attatch_more_photo = ['path' => 'enable_attatch_more_photo', 'value' => $request->enable_attatch_more_photo];
            $this->db->updateCollection($_id, $enable_attatch_more_photo);
            $enable_attatch_more_photo_is_required = ['path' => 'enable_attatch_more_photo_is_required', 'value' => $request->enable_attatch_more_photo_is_required];
            $this->db->updateCollection($_id, $enable_attatch_more_photo_is_required);
            $enable_camera = ['path' => 'enable_camera', 'value' => $request->enable_camera];
            $this->db->updateCollection($_id, $enable_camera);
            $enable_camera_watermark = ['path' => 'enable_camera_watermark', 'value' => $request->enable_camera_watermark];
            $this->db->updateCollection($_id, $enable_camera_watermark);
            $enable_objective = ['path' => 'enable_objective', 'value' => $request->enable_objective];
            $this->db->updateCollection($_id, $enable_objective);
            $enable_print_button = ['path' => 'enable_print_button', 'value' => $request->enable_print_button];
            $this->db->updateCollection($_id, $enable_print_button);
            $enable_send_notify_to_mooban = ['path' => 'enable_send_notify_to_mooban', 'value' => $request->enable_send_notify_to_mooban];
            $this->db->updateCollection($_id, $enable_send_notify_to_mooban);
            $enable_signpad = ['path' => 'enable_signpad', 'value' => $request->enable_signpad];
            $this->db->updateCollection($_id, $enable_signpad);
            $enable_stamp = ['path' => 'enable_stamp', 'value' => $request->enable_stamp];
            $this->db->updateCollection($_id, $enable_stamp);
            $enable_use_coupon = ['path' => 'enable_use_coupon', 'value' => $request->enable_use_coupon];
            $this->db->updateCollection($_id, $enable_use_coupon);
            $res = [
                'status' => true,
                'data' => $request->input()
            ];
            $status_code = 200;
        } else if ($request->_tab === "3") {
            $addition_fields = ['path' => 'addition_fields', 'value' => $request->addition_fields];
            $this->db->updateCollection($_id, $addition_fields);
            $res = [
                'status' => true,
                'data' => $request->input()
            ];
            $status_code = 200;
        } else if ($request->_tab === "4") {
            $calculation_js = ['path' => 'calculation_js', 'value' => $request->calculation_js];
            $this->db->updateCollection($_id, $calculation_js);
            $res = [
                'status' => true,
                'data' => $request->input()
            ];
            $status_code = 200;
        } else {
            $res = [
                'status' => false,
            ];
            $status_code = 400;
        }
        return $this->sendResponse($res, $status_code);
    }
}