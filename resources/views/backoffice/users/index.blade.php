@extends('layouts.backoffice.app')
@push('css_lib')
    @include('layouts.datatables_css')
    <style>
        .page-item:first-child .page-link {
            margin-left: 0;
            border-top-left-radius: .25rem;
            border-bottom-left-radius: .25rem;
            width: auto !important;
        }

        .page-item:last-child .page-link {
            border-top-right-radius: .25rem;
            border-bottom-right-radius: .25rem;
            width: auto !important;
        }

        .animated_link2 a {
            position: relative;
            color: white;
        }

        .animated_link2 a:hover {
            color: white;
        }

        .animated_link2 a:before {
            content: "";
            position: absolute;
            width: 100%;
            height: 2px;
            bottom: 0;
            left: 0;
            background-color: #333;
            visibility: hidden;
            -webkit-transform: scaleX(0);
            transform: scaleX(0);
            -webkit-transition: all 0.3s ease-in-out 0s;
            transition: all 0.3s ease-in-out 0s;
        }

        .animated_link2 a:hover:before {
            visibility: visible;
            background-color: #f2f2f2;
            -webkit-transform: scaleX(1);
            transform: scaleX(1);
        }
        .alert2{
            position: absolute;
            top: 5px;
            right:2%;
            width: auto !important;
            padding: .75rem 1.25rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: .25rem;
        }
        .alert-success-custom {
            color: #20993c;
            background-color: #d4edda;
            border-color: #c3e6cb;
        }
        .alert3{
            position: absolute;
            top: 5px;
            right:2%;
            width: auto !important;
            padding: .75rem 1.25rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: .25rem;
        }
        .alert-danger-custom {
            color: #721c24;
            background-color: #f8d7da;
            border-color: #f5c6cb;
        }
    </style>
    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">--}}
    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">--}}
@endpush
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-bold">User
                        <small class="mx-3">|</small><small>ข้อมูลผู้ใช้งาน</small>
                    </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb bg-white float-sm-right rounded-pill px-4 py-2 d-none d-md-flex">
                        <li class="breadcrumb-item active"><a href="{{url('/backoffice-management-systems/users')}}">
                                <i class="fas fa-folder-open"></i>
                                ข้อมูลผู้ใช้งาน</a>
                        </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="alert2 alert-success-custom fade show alert-dismissible" id="alert-success" role="alert" style="display: none;">
        <strong><i class="fa fa-check-circle" aria-hidden="true"></i></strong>
        Update data <b>success</b> message!
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="alert3 alert-danger-custom fade show alert-dismissible" id="alert-danger" role="alert" style="display: none;">
        <strong><i class="fa fa-warning" aria-hidden="true"></i></strong>
        Update data <b>Fail</b> message!
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="content">
        <div class="clearfix"></div>
        <div class="card shadow-sm">
            <div class="card-header">
                <ul class="nav nav-tabs d-flex flex-md-row flex-column-reverse align-items-start card-header-tabs">
                    <div class="d-flex flex-row">
                        <li class="nav-item">
                            <a class="nav-link active" href="{!! url()->current() !!}">
                                <i class="fa fa-list mr-2"></i>ผู้ใช้งานทั้งหมด</a>
                        </li>
                    </div>
                </ul>
            </div>
            <div class="card-body">
                <button name="action"
                        title="เพิ่มผู้ใช้งาน"
                        onclick="viewCreateUser()"
                        class="btn btn-success btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-plus-circle ew-icon"></i>
                                        </span>
                    เพิ่มผู้ใช้งาน
                </button>
                <br>
                <br>
                <table id="tb-customer" class="table dataTable no-footer" style="width:100%">
                    <thead>
                    <tr>
                        <th>ลำดับ</th>
                        <th>ชื่อผู้ใช้งาน</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($db_users as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td class="animated_link2">
                                {{  $item["id"]  }}
                            </td>
                           <td>
                               <a href="{{ url('/backoffice-management-systems/users/'. $item["id"]) }}"
                                  class="btn btn-info btn-xs">
                                        <span class="btn-label">
                                        <i class="fas fa-edit ew-icon"></i>
                                        </span>
                                   Setting
                               </a>
                           </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>ลำดับ</th>
                        <th>ชื่อผู้ใช้งาน</th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    {{-- Create user   --}}
    <div class="modal fade" id="createUserModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header no-bd">
                    <h5 class="modal-title">
                        <span class="fw-mediumbold">เพิ่ม</span>
                        <span class="fw-light">ผู้ใช้งาน</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group" align="center">
                                    <input id="username" name="username" type="text" class="form-control"
                                           placeholder="กรุณากรอกผู้ใช้งาน">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer no-bd">
                    <button type="button" class="btn btn-outline-success" onclick="addUserDB()">
                        บันทึกข้อมูล
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts_lib')
    {{--    @include('layouts.datatables_js')--}}
    <script type="text/javascript" src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('vendor/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('vendor/datatables-colreorder/js/dataTables.colReorder.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('vendor/datatables-rowgroup/js/dataTables.rowGroup.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('vendor/datatables-rowgroup/js/rowGroup.bootstrap4.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('vendor/datatables-fixedcolumns/js/dataTables.fixedColumns.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('vendor/datatables-fixedcolumns/js/fixedColumns.bootstrap4.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('vendor/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('vendor/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script>
        //====================== USER ================================
        $(document).ready(function () {
            $('#tb-customer').DataTable({
                "processing": true,
                "responsive": true,
                "autoWidth": false,
                "oLanguage": {
                    "sSearch": "ค้นหา:   "
                }
            });
        });

        function viewCreateUser()
        {
            $('#createUserModal').modal('show');
        }
        function addUserDB()
        {
            const username = $("#username").val();
            if (username) {
                $.ajax({
                    url: '{{ url('/backoffice-management-systems/users') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'username': username,
                        'collection': 'user',
                        "_token": "{{ csrf_token() }}",
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        console.log('success:', response.data.status);
                        if (response.data.status === true) {
                            swal("บันทึกข้อมูล", "เรียบร้อย !", "success");
                            window.setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        swal("เกิดข้อผิดพลาด", data + "<br> :)", "error");
                        window.setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                });
            } else {
                alert("กรุณากรอกข้อมูล !")
            }
        }
        //====================== USER ================================
    </script>
@endpush

