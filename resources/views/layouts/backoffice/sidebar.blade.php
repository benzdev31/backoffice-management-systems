<!-- Main Sidebar Container -->
{{--<aside class="main-sidebar sidebar-{{setting('theme_contrast')}}-{{setting('theme_color')}} shadow">--}}
<aside class="main-sidebar sidebar-light-dark shadow">
    <!-- Brand Logo -->
    <a href="{{ url('backoffice-management-systems')}}"
       class="brand-link border-bottom-0 {{setting('logo_bg_color','text-light  navbar-navy')}}">
        <img src="{{$app_logo ?? ''}}" alt="{{setting('app_name')}}" class="brand-image">
        {{--        <span class="brand-text font-weight-light">{{ env('app_name')}}</span> </a>--}}
        <span class="brand-text font-weight-light">backoffice-koder3</span> </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-flat" data-widget="treeview" role="menu"
                data-accordion="false">
                <li class="nav-header">{{trans('lang.app_management')}}</li>
                <li class="nav-item">
                    <a class="nav-link
                        {{ (Request::is('backoffice-management-systems') || Request::is('backoffice-management-systems/customer/*') || Request::is('backoffice-management-systems/customer')) ? 'active' : '' }}"
                       href="{!! url('backoffice-management-systems/customer') !!}">
                        <i class="nav-icon fas fa-folder-open"></i>
                        <p>ลูกค้า</p>
                    </a>
                </li>
                <li class="nav-item has-treeview {{ (Request::is('backoffice-management-systems/users*')) ? 'menu-open' : '' }}">
                    <a href="{{ url('backoffice-management-systems/users') }}"
                       class="nav-link {{ (Request::is('backoffice-management-systems/user*')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-users-cog"></i>
                        <p>User <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a class="nav-link
                            {{ (Request::is('backoffice-management-systems/users*')) ? 'active' : '' }}"
                            href="{!! url('backoffice-management-systems/users') !!}">
                            <i class="nav-icon fas fa-map-marked-alt"></i>
                            <p>ข้อมูลผู้ใช้งาน</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
