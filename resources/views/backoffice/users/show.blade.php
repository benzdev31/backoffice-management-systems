@extends('layouts.backoffice.app')
@push('css_lib')
    <style>
        .col-form-label2 {
            padding-top: calc(.375rem + 1px);
            padding-bottom: calc(.375rem + 1px);
            margin-bottom: 0;
            font-size: small;
            line-height: 1.5;
        }
    </style>
    @include('layouts.datatables_css')
    @include('layouts.custom_css')
    <style>
        #info-block section {
            border: 1px solid black;
        }

        .file-marker > div {
            padding: 0 3px;
            height: auto;
            margin-top: -0.8em;

        }

        .box-title {
            background: white none repeat scroll 0 0;
            display: inline-block;
            padding: 0 2px;
            margin-left: 8em;
        }

        /*  switch input  */
        /*
    get rid of the fieldset styling and keep
    this all on a single line
*/
        .radio-switch {
            border: none;
            padding: 0;
            white-space: nowrap;
        }

        /*
            radio button groups often benefit from a legend to
            provide context as to what the different
            options pertain to. Ideally this would be visible to all
            users, but you know...
        */
        .radio-switch legend {
            font-size: 2px;
            opacity: 0;
            position: absolute;
        }

        /*
            relative labels to help position the pseudo elements
            the z-index will be handy later, when the labels that
            overlap the visual switch UI need to be adjusted
            to allow for a user to toggle the switch without
            having to move their mouse/finger to the different
            sides of the UI
        */
        .radio-switch label {
            display: inline-block;
            line-height: 2;
            position: relative;
            z-index: 2;
        }

        /*
            inputs set to opcacity 0 are still accessible.
            Apparently there can be issues targetting inputs with
            Dragon speech recognition software if you use the typical
            'visually-hidden' class...so might as well just avoid that issue...
        */
        .radio-switch input {
            opacity: 0;
            position: absolute;
        }

        /*
            a 2 option toggle can only have 2 options...so instead of
            adding more classes, i'm just going to use some
            structural pseudo-classes to target them...
            cause why let all that good work go to waste?!

          the large padding is used to position the labels
          on top of the visual UI, so the switch UI itself
          can be mouse clicked or finger tapped to toggle
          the current option
        */
        .radio-switch label:first-of-type {
            padding-right: 5em;
        }

        .radio-switch label:last-child {
            margin-left: -4.25em;
            padding-left: 5em;
        }

        /*
            oh focus within, I can't wait for you to have even more support.
            But you'll never be in IE11, so we're going to need a
            polyfill for you for a bit...
         */
        .radio-switch:focus-within label:first-of-type:after {
            box-shadow: 0 0 0 2px #fff, 0 0 0 4px #2196f3;
        }

        /* polyfill class*/
        .radio-switch.focus-within label:first-of-type:after {
            box-shadow: 0 0 0 2px #fff, 0 0 0 4px #2196f3;
        }

        /* making the switch UI.  */
        .radio-switch label:first-of-type:before,
        .radio-switch label:first-of-type:after {
            border: 1px solid #aaa;
            content: "";
            height: 2em;
            overflow: hidden;
            pointer-events: none;
            position: absolute;
            vertical-align: middle;
        }

        .radio-switch label:first-of-type:before {
            background: #fff;
            border: 1px solid #aaa;
            border-radius: 100%;
            position: absolute;
            right: -.075em;
            transform: translateX(0em);
            transition: transform .2s ease-in-out;
            width: 2em;
            z-index: 2;
        }

        .radio-switch label:first-of-type:after {
            background: #222;
            border-radius: 1em;
            margin: 0 1em;
            transition: background .2s ease-in-out;
            width: 4em;
        }

        /*
            Visually change the switch UI to match the
            checked state of the first radio button
        */
        .radio-switch input:first-of-type:checked ~ label:first-of-type:after {
            background: #2196f3;
        }

        .radio-switch input:first-of-type:checked ~ label:first-of-type:before {
            transform: translateX(-2em);
        }

        /* Move the 2nd label to have a lower z-index, so when that
           option is toggled, the first label will overlay on top of the
           Switch ui, and the switch can be pressed again to toggle back
           to the prevoius state. */
        .radio-switch input:last-of-type:checked ~ label:last-of-type {
            z-index: 1;
        }

        /*
          system defaut fonts sure do load quick...
        */


    </style>
@endpush
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-bold">User
                        <small class="mx-3">|</small><small>ข้อมูลผู้ใช้งาน | {{ $id }}</small>
                    </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb bg-white float-sm-right rounded-pill px-4 py-2 d-none d-md-flex">
                        <li class="breadcrumb-item active"><a href="{{url('/backoffice-management-systems/users')}}">
                                <i class="fas fa-folder-open"></i>
                                ข้อมูลผู้ใช้งาน</a>
                        </li>
                        <li class="breadcrumb-item active">
                            รายการข้อมูล {{ $id }}
                        </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="content">
        <div class="clearfix"></div>
        <div class="card shadow-sm">
            <div class="card-header">
                <ul class="nav nav-tabs  d-flex flex-md-row flex-column-reverse align-items-start card-header-tabs"
                    role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">
                            <i class="nav-icon fas fa-cog"></i> ตั้งค่าทั่วไป
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">
                            <i class="nav-icon fas fa-cog"></i> เปิด/ปิด การกรอกข้อมูล</a>
                    </li>
                </ul><!-- Tab panes -->
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tabs-1" role="tabpanel">
                        <div class="row mb-2">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label for="inputPassword" style="text-align: center;"
                                           class="col-sm-4 col-form-label2">โครงการ
                                    </label>
                                    <div class="col-sm-8">
                                        @if(!empty($db_doc_user['data']['customer_name']))
                                            {{ $db_doc_user['data']['customer_name'] }}
                                        @else
                                            <div class="col-sm-8">
                                                <select class="form-control" id="customer_name"
                                                        name="customer_name">
                                                    <option value="false" selected>--- กรุณาเลือก ---</option>
                                                    @foreach($items as $item)
                                                        <option value="{{ $item['id'] }}">{{ $item['id'] }}</option>
                                                    @endforeach
                                                </select>
                                                <br>
                                                <button class="btn btn-success" type="button"
                                                        onclick="saveDB(2)"
                                                ><i class="fas fa-save"></i>
                                                    บันทึก
                                                </button>
                                            </div>
                                            <hr>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" style="text-align: center;"
                                           class="col-sm-4 col-form-label2">หัวข้อขอความช่วยเหลือ
                                    </label>
                                    <div class="col-sm-8">
                                        @foreach($db_doc_user['data']['request_help_topic'] as $key => $value)
                                            - {{ $value }} <br>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.row -->
                    </div>
                    <div class="tab-pane" id="tabs-2" role="tabpanel">
                        <div class="row mb-2">

                        </div><!-- /.row -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts_lib')
    @include('layouts.custom_js')

    <script>
        //====================== USER/SHOW/{id} ================================

        //====================== USER/SHOW/{id} ================================
    </script>
@endpush

