@extends('layouts.auth.auth-backoffice')
@section('content')
    @if(session()->has('error'))
        <style>
            .alert{
                position: fixed;
                top: 5px;
                right:2%;
            }
        </style>
        <div class="alert alert-danger" role="alert">
            <button type="button"
                    class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ session()->get('error') }}</strong>
        </div>
        <script>
            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove();
                });
            }, 2000);
        </script>
    @endif


    <div class="card-body login-card-body">
        <p class="login-box-msg">{{__('auth.login_title')}}</p>

        <form action="{{ url('/backoffice-management-systems/login') }}" method="post">
            {!! csrf_field() !!}

            <div class="input-group mb-3">
                <input value="{{ old('email') }}"
                       type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                       name="email" placeholder="{{ __('auth.email') }}" aria-label="{{ __('auth.email') }}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                </div>
                @if ($errors->has('email'))
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                @endif
            </div>

            <div class="input-group mb-3">
                <input value="{{ old('password') }}" type="password" class="form-control
                {{ $errors->has('password') ? ' is-invalid' : '' }}"
                       name="password" placeholder="{{__('auth.password')}}" aria-label="{{__('auth.password')}}">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fas fa-lock"></i></span>
                </div>
                @if ($errors->has('password'))
                    <div class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </div>
                @endif
            </div>
            <div class="row mb-2">
                <div class="col-8">
                    <div class="icheck-primary">
                        <input type="checkbox" id="remember" name="remember"> <label for="remember">
                            {{__('auth.remember_me')}}
                        </label>
                    </div>
                </div>
                <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block">{{__('auth.login')}}</button>
                </div>
            </div>
        </form>
    </div>

@endsection


