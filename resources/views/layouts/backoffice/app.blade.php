<!DOCTYPE html>
<html lang="{{setting('language','en')}}">
<head>
    <meta charset="UTF-8">
    <title>{{setting('app_name')}} | {{setting('app_short_description')}}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="icon" type="image/png" href="{{$app_logo ?? ''}}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('vendor/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/styles.min.css')}}">
    @stack('css_lib')
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/'.setting("theme_color","dark").'.min.css')}}">
{{--    <link rel="stylesheet" href="{{asset('css/dark.min.css')}}">--}}
    @yield('css_custom')
    <link rel="stylesheet" href="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css">
</head>

<body class="@if(in_array(setting('language','en'), ['ar','ku','fa','ur','he','ha','ks'])) rtl @else ltr @endif layout-fixed
{{setting('fixed_header',false) ? "layout-navbar-fixed" : ""}} {{setting('fixed_footer',false) ? "layout-footer-fixed" : ""}} sidebar-mini {{setting('theme_color')}} {{setting('theme_contrast','')}}-mode" data-scrollbar-auto-hide="l" data-scrollbar-theme="os-theme-dark">
<div class="wrapper">
    <nav class="main-header navbar navbar-expand {{setting('nav_color','navbar-dark navbar-navy')}} border-bottom-0">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <img src="{{ asset('images/avatar_default.png') }}" class="brand-image mx-2 img-circle elevation-2" alt="User Image">
                    <i class="fa fas fa-angle-down"></i> {!! session('user') !!}
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="{!! url('/logout') !!}" class="dropdown-item">
                        <i class="fas fa-envelope mr-2"></i>
                        {{__('auth.logout')}}
                    </a>
{{--                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">--}}
{{--                        {{ csrf_field() }}--}}
{{--                    </form>--}}
                </div>
            </li>
        </ul>
    </nav>

    <!-- Left side column. contains the logo and sidebar -->
@include('layouts.backoffice.sidebar')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
    </div>

    <!-- Main Footer -->
    <footer class="main-footer border-0 shadow-sm">
        <strong>Copyright © 2021 </strong> Koder3
    </footer>

</div>

<!-- jQuery -->
<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>

<script src="{{asset('vendor/bootstrap-v4-rtl/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>

<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="{{asset('https://www.gstatic.com/firebasejs/7.2.0/firebase-app.js')}}"></script>

<script src="{{asset('https://www.gstatic.com/firebasejs/7.2.0/firebase-messaging.js')}}"></script>

<script type="text/javascript">@include('vendor.notifications.init_firebase')</script>

<script type="text/javascript">

</script>

@stack('scripts_lib')
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<script src="{{asset('js/scripts.min.js')}}"></script>
@stack('scripts')
<script type="text/javascript"
        src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js"></script>

</body>
</html>
