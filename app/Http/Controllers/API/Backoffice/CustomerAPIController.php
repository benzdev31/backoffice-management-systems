<?php


namespace App\Http\Controllers\API\Backoffice;


use App\Firebase\Firestore;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class CustomerAPIController extends Controller
{
    public function collectionDetail(Request $request)
    {
        $name = $request->name ?? null; // Get the input value
        $collection = $request->collection ?? null; // Get the input value
        if ($name !== null) {
            $fireStore = new Firestore($collection);
            $docData = $fireStore->getDoc($name);
            $options = [];
            $i = 0;
            foreach ($docData["data"] as $key => $value) {
                $options[$i]['name'] = $key;
                $options[$i]['value'] = $value;
                $i++;
            }
            $data = [
                'collection' => $collection,
                'id' => $name,
                'data' => $options
            ];
            return $this->sendResponse($data, 200);
        }
    }
    public function updateCollection(Request $request,string $id)
    {

        $name = $request->field_name;
        $value = $request->field_value;
        $data = ['path' => $name, 'value' => $value];
        $firestore = new Firestore('customer');
        $update = $firestore->updateCollection($id,$data);
        $status_code = null;
        if ($update) {
            $res = [
                'status' => true,
                'status_db' => $update,
                'data' => $request->input()
            ];
            $status_code = 200;
        } else {
            $res = [
                'status' => false,
                'status_db' => $update
            ];
            $status_code = 400;
        }
        return $this->sendResponse($res, $status_code);
    }
}